const db = firebase.firestore();
    
const insumoForm = document.getElementById("insumo-form");
const insumoContainer = document.getElementById("Insumos-container");

let editStatus = false;
let id = '';
/**
 * Save a New Task in Firestore
 * @param {string} nombre 
 * @param {string} cantidad
 */

    const saveInsumo = (nombre, cantidad) =>
    db.collection("Insumos").doc().set({
        nombre,
        cantidad,
    });
    const getInsumos = () => db.collection("Insumos").get();

    const onGetInsumos = (callback) => db.collection("Insumos").onSnapshot(callback);

    const deleteInsumo = (id) => db.collection("Insumos").doc(id).delete();

    const getInsumo = (id) => db.collection("Insumos").doc(id).get();

    const updateInsumo = (id, updatedInsumo) => db.collection('Insumos').doc(id).update(updatedInsumo);


window.addEventListener("DOMContentLoaded", async e => {
    onGetInsumos((querySnapshot) => {
        insumosContainer.innerHTML = "";

        querySnapshot.forEach((doc) => {
            const insumo = doc.data();

            insumosContainer.innerHTML += `<div class ="card card-body mt-2 border-primary">
        <h3 class="h5">${insumo.nombre}</h3>
        <p>${insumo.cantidad}</p>
        <div>
            <button class="btn btn-primary btn-delete" data-id="${doc.id}">
          </button>
          <button class="btn btn-secondary btn-edit" data-id="${doc.id}">
            </button>
        </div>
    </div>`;
    });
            const btnsDelete = insumosContainer.querySelectorAll(".btn-delete");
            btnsDelete.forEach((btn) =>
                btn.addEventListener("click", async (e) => {
                    console.log(e.target.dataset.id);
                        try {
                            await deleteInsumo(e.target.dataset.id);
                            } catch (error) {
                            console.log(error);
                        }
                    })
            );
            const btnsEdit = insumosContainer.querySelectorAll(".btn-edit");
            btnsEdit.forEach((btn) => {
                btn.addEventListener("click", async (e) => {
                    try {
                        const doc = await getInsumo(e.target.dataset.id);
                        const insumo = doc.data();
                    insumoForm["insumo-nombre"].value = insumo.nombre;
                    insumoForm["insumo-cantidad"].value = insumo.cantidad;

                    editStatus = true;
                    id = doc.id;
                    insumoForm["btn-insumo-form"].innerText = "Actualizar";

        } catch (error) {
          console.log(error);
        }
      });
    });
  });
});
insumoForm.addEventListener("submit", async (e) => {
    e.preventDefault();
  
    const nombre = insumoForm["insumo-nombre"];
    const cantidad = insumoForm["insumo-cantidad"];
  
    try {
      if (!editStatus) {
        await saveInsumo(nombre.value, cantidad.value);
      } else {
        await updateInsumo(id, {
            nombre: nombre.value,
            cantidad: cantidad.value,
        })
  
        editStatus = false;
        id = '';
        insumoForm['btn-insumo-form'].innerText = 'Guardar';
      }
  
      insumoForm.reset();
      nombre.focus();
    } catch (error) {
      console.log(error);
    }
  });